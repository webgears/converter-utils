#!/usr/bin/env bash

docker build -t docker.webgears3d.com/converters/webgears/converter-utils:latest . \
    && docker push docker.webgears3d.com/converters/converter-utils:latest
