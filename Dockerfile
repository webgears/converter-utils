FROM debian:latest

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends ruby \
    && rm -rf /var/lib/apt/lists/*

RUN gem install \
    -q --no-rdoc --no-ri --no-format-executable --no-user-install \
    bunny --version ">= 2.6.4"

COPY . /var/converters/utils