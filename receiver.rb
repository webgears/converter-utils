#!/usr/bin/env ruby
# encoding: utf-8

require 'bunny'
require 'logger'
require 'json'
require 'open3'

def consume(conn, queue_name, cmd)
  logger = Logger.new(STDERR)
  channel = conn.create_channel
  queue = channel.queue(queue_name, durable: true)
  channel.prefetch(1)
  logger.info(" [x] Waiting for messages...")
  queue.subscribe(block: true) do |dinfo, _properties, body|
    logger.info(" [x] Received message '#{body}'!")
    args = JSON.parse(body)
    if args['cmd']
      input_cmd = args['cmd']
    else
      args['input'] = resolve_input(args['input'])
      args['output'] = resolve_output(args['output'])
      args['options'] = resolve_options(args['options'])
      input_cmd = cmd.call(args)
    end
    logger.info(" ARGS #{args}")
    logger.info(" [x] cmd '#{input_cmd}'!")
    stdout, stderr, status = Open3.capture3(input_cmd)
    logger.info( " Open3 finished with stdout #{stdout} and stderr #{stderr} ")
    begin
      x = channel.default_exchange
      message = JSON.generate({
                                         error: status.exitstatus.to_s,
                                         stderr: stderr,
                                         stdout: stdout,
                                         status: status.to_s
                                     })
      x.publish(
          message,
          routing_key: _properties.reply_to,
          correlation_id: _properties.correlation_id
      )
      logger.info( " publish message to queue with message #{message}, routing_key #{_properties.reply_to}, correlation_id #{_properties.correlation_id} ")
    rescue StandardError => e
      logger.info(e.message)
      logger.info(e.backtrace.inspect)
      logger.info( " SOMETHING WENT WRONG with message #{message}, routing_key #{_properties.reply_to}, correlation_id #{_properties.correlation_id} ")
    end
  end
end

def resolve_input(input)
  result = ''
  input.each do |x|
    if x['key'].empty?
      result << x['value'] + " "
      next
    end
    result << "--#{x['key']}=#{x['value']} "
  end
  result
end

def resolve_output(output)
  result = ''
  output.each do |x|
    if x['key'].empty?
      result << x['value'] + " "
      next
    end
    result << "--#{x['key']}=#{x['value']} "
  end
  result
end

def resolve_options(options)
  result = options
      .map {|key, value| "--#{key}=#{value}"}
      .join(' ')
  result
end

def resolve_cmd(cmd_string)
  unless cmd_string.empty?
    return Proc.new do |args|
      cmd_string.gsub(/\$([\w\d_\-\/]+)/) {|_match| args.fetch($1)}
    end
  end
  Proc.new do |args|
    "#{args['executable']} #{args['options']} #{args['input']} #{args['output']}"
  end
end

def main(queue_name, cmd)
  conn = Bunny.new(hostname: 'rabbitmq')
  conn.start
  consume(conn, queue_name, cmd)
end

queue_name = ARGV[0]
cmd_string = ARGV.drop(1).join(' ')

main(queue_name, resolve_cmd(cmd_string))
