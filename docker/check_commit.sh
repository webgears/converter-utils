#!/usr/bin/env bash

FILE=commithash.txt
BITBUCKET_URL=https://bitbucket.org/webgears/converter-utils

function check_commit {
    new_commit=$(git ls-remote $BITBUCKET_URL | grep refs/heads/master | cut -f 1)
    if [[ -f "$FILE" ]]; then
        old_commit=$(cat commithash.txt)
        if [[ $old_commit == $new_commit ]]; then
            echo "commits equal"
        else
            save_commit $new_commit
        fi
    else
        save_commit $new_commit
    fi
}

function save_commit {
    echo "commits unequal; update needed"
    rm -rf $FILE
    echo $1 > $FILE
}

check_commit
