#!/usr/bin/env bash

apt-get update \
	&& apt-get install -y --no-install-recommends wget \
	tar \
	curl \
    && apt-get install amqp-tools -y \
	&& apt-get -y autoremove \
	&& rm -rf /var/lib/apt/lists/*

mkdir ~/temp \
    && cd ~/temp \
    && wget https://cmake.org/files/v3.10/cmake-3.10.0.tar.gz \
    && tar xzvf cmake-3.10.0.tar.gz \
    && cd cmake-3.10.0/ \
    && ./bootstrap --system-curl\
    && make -j4 \
    && make install \
    && rm -rf ~/temp

apt-get install zlib1g-dev libssl-dev libreadline-dev libgdbm-dev openssl \
    && mkdir ~/RUBY \
    && cd ~/RUBY \
    && wget https://cache.ruby-lang.org/pub/ruby/2.3/ruby-2.3.0.tar.gz \
    && tar xvfz ruby-2.3.0.tar.gz \
    && cd ruby-2.3.0 \
    && ./configure \
    && make \
    && make install \
    && rm -rf ~/RUBY

RUN gem install \
    -q --no-rdoc --no-ri --no-format-executable --no-user-install \
    bunny --version ">= 2.6.4"
